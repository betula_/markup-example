
module.directive('imgBlank', [function() {
  return {
    restrict: 'A',
    link: function (scope, element) {
      element.attr('src', 'data:image/gif;base64,R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAAAICTAEAOw==');
    }
  }
}]);

