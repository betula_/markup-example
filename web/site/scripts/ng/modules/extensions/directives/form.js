module.directive('formIsolate', [function() {
	return {
		restrict: 'A',
		priority: 100,
		require: 'form',
		scope: true,
		compile: function(element, attrs) {
			var name = attrs.name || attrs.ngForm;
			if (!name) {
				attrs.$set('name', '$form');
			}
		}
	}
}]);

module.directive('formPlain', [function() {
	return {
		restrict: 'A',
		priority: 99,
		require: 'form',
		compile: function(element, attrs) {
			var name = attrs.name || attrs.ngForm;
			if (!name) {
				attrs.$set('name', '$form');
			}
		}
	}
}]);