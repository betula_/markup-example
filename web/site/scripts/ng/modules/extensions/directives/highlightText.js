module.directive('highlightText', [function() {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {

			attrs.$observe('highlightText', function() {
				refresh();
			});
			attrs.$observe('highlightTextNeedle', function() {
				refresh();
			});

			function refresh() {
				var body = String(attrs.highlightText || '');
				var needle = String(attrs.highlightTextNeedle || '');
				if (needle) {
					var pattern = needle.replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}=!<>\|:])/g, "\\$1");
					body = body.replace(new RegExp(pattern, 'gi'), function(m) {
						return '<span class="ng-highlight">' + m + '</span>';
					});
				}
				element.html(body);
			}

		}
	}
}]);
