
module.directive('clickAjax', ['$http', '$location', 'jQuery', function($http, $location, jQuery) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			element.on('click', function() {
				scope.$apply(function() {
					var data = {};
					if (attrs.clickAjaxData) {
						data = scope.$eval(attrs.clickAjaxData);
					}
					var url = attrs.clickAjax;
					var method = String(attrs.method || attrs.clickAjaxMethod).toLowerCase();
					if (['get', 'post', 'put', 'delete', 'head', 'jsonp'].indexOf(method) == -1) method = 'get';

					var config = {
						url: url,
						method: method
					};
					if (method == 'get') {
						config.params = data;
					} else {
						config.data = data;
					}
					var promise = $http(config);
					promise.then(function(response) {
						if (attrs.clickAjaxSuccess) {
							scope.$eval(attrs.clickAjaxSuccess, {
								$request: data,
								$response: response,
								$data: response.data
							});
						}
					}, function(response) {
						if (attrs.clickAjaxError) {
							scope.$eval(attrs.clickAjaxError, {
								$request: data,
								$response: response,
								$data: response.data
							});
						}
					});
				});
			});
		}
	}
}]);