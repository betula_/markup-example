
module.directive('textTransformLower', [function() {
	function isEmpty(value) {
		return typeof value == 'undefined' || value === '' || value === null || value !== value;
	}
	return {
		require: 'ngModel',
		restrict: 'A',
		link: function(scope, element, attrs, ctrl) {
			ctrl.$parsers.unshift(function(viewValue) {
				if (isEmpty(viewValue) || typeof viewValue != 'string') {
					return viewValue;
				}
                viewValue = viewValue.toLowerCase();
				ctrl.$viewValue = viewValue;
				ctrl.$render();
				return viewValue;
			});
		}
	}
}]);

module.directive('textTransformUpper', [function() {
	function isEmpty(value) {
		return typeof value == 'undefined' || value === '' || value === null || value !== value;
	}
	return {
		require: 'ngModel',
		restrict: 'A',
		link: function(scope, element, attrs, ctrl) {
			ctrl.$parsers.unshift(function(viewValue) {
				if (isEmpty(viewValue) || typeof viewValue != 'string') {
					return viewValue;
				}
				viewValue = viewValue.toUpperCase();
				ctrl.$viewValue = viewValue;
				ctrl.$render();
				return viewValue;
			});
		}
	}
}]);

