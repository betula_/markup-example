

module.directive('equalsValidator', [function() {
	function isEmpty(value) {
		return typeof value == 'undefined' || value === '' || value === null || value !== value;
	}
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, element, attrs, model) {

			var validator = function(value) {
				if (!isEmpty(value)) {
					var valid = scope.$eval(attrs.equalsValidator) == value;
					model.$setValidity('equals', valid);
					if (!valid) {
						return undefined;
					}
				}
				model.$setValidity('equals', true);
				return value;
			};

			model.$parsers.push(validator);
			model.$formatters.push(validator);

			scope.$watch(attrs.equalsValidator, function(value) {
				model.$setValidity('equals', model.$viewValue == value);
			});
		}
	}
}]);

