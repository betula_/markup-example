/**
 * @name ng.directive:tabs
 * @restrict A
 *
 * @description
 * Внешний контейнер для таб панели, он делает возможным создание табов, а также через него устанавливается
 * связь с моделью и некоторые другие общие параметры.
 * Важно заметить, что по умолчанию в случае если активный таб не будет определен им станет первый таб в панеле.
 *
 * Когда табы и панели добавляются, в случае если у них не указаны явно id они определятся в порядке очереди по
 * последовательности 1,2,3... важно отметить, что отчет начинается от 1.
 * Табы и панели имеют отдельные счетчики потому каждому i-ому табу сооветствует i-ая панел.
 *
 * @param {string} tabs= если указано, то это модель которая будет служить источником данных для табов,
 *  если не указано, значит табы будут работать без модели
 * @param {string} [tabsClassActive] класс активного таба, который будет использован если не переопределен
 *  непосредственно в табе
 * @param {boolean} [tabsNonDefault] если указано true, то по умолчанию не будет выбран первый таб, т.е.
 *  если модель или табы никто не определит активного таба, то его не будет вообще.
 *
 * @example
 * Без связи с моделью
 <div tabs="">
 </div>
 * Со связью с моделью
 <div tabs="model">
 </div>
 */

/**
 * @name ng.directive:tab
 * @restrict A
 * @require ^tabs
 *
 * @description
 * Собственно таб, если его идентификатор не определн, то tabs присвоет ему идентификатор автоматически
 *
 * @param {string} tab= идентификатор таба
 * @param {string} [tabClassActive] класс который будет добавлятся в активный таб, если он не указан будет
 *  браться из tabs.tabsClassActive
 * @param {boolean} [tabActive] если таб помечен как активный, то он станет активным в случае если в контейнере
 *  нет модели или она определна как неопределенная.
 *
 * @example
 * Без связи с моделью
 <div tabs="">
 <a tab="">First</a>
 <a tab="" tab-active="true">Second</a>
 </div>
 * Со связью с моделью
 <div tabs="model">
 <a tab="value1">First</a>
 <a tab="">Second</a>
 </div>
 */

/**
 * @name ng.directive:tab-pane
 * @restrict A
 * @require ^tabs
 *
 * @description
 * Таб панель, если его идентификатор не определн, то tabs присвоет ему идентификатор автоматически
 *
 * @param {string} tabPane= идентификатор таб панели
 * @param {boolean} [tabPaneActive] если таб панель помечена как активная, то она станет активной в случае если в
 *  контейнере нет модели или она определна как неопределенная.
 *
 * @example
 * Без связи с моделью
 <div tabs="">
 <a tab="">First</a>
 <a tab="">Second</a>
 <div tab-pane="">First pane</div>
 <div tab-pane="" tab-pane-active="true">Second pane</div>
 </div>
 * Со связью с моделью
 <div tabs="model">
 <a tab="value1">First</a>
 <a tab="">Second</a>
 <div tab-pane="">Second</div>
 <div tab-pane="value1">First</div>
 </div>
 */



module.directive('tabs', ['$parse', function($parse) {
	function isEmpty(value) {
		return typeof value == 'undefined' || value === '' || value === null || value !== value;
	}
	return {
		restrict: 'A',
		require: 'tabs',
		link: function(scope, element, attr, tabs) {
			var i;
			if (!isEmpty(attr.tabsClassActive)) {
				tabs.defaultClassActive = attr.tabsClassActive;
				for (i = 0; i < tabs.tabs.length; i++) {
					tabs.tabs[i].classActive = tabs.tabs[i].classActive || tabs.defaultClassActive;
				}
			}
			if (attr.tabs) {
				var getter = $parse(attr.tabs);
				var id = tabs.active();
				tabs.active = function(id) {
					if (typeof id != 'undefined' && id != getter(scope)) {
						scope.$evalAsync(function() {
							getter.assign(scope, id);
						});
						return id;
					}
					return getter(scope);
				};
				if (isEmpty(tabs.active()) && !isEmpty(id)) {
					tabs.active(id);
				}
				scope.$watch(attr.tabs, function(id) {
					tabs.activate(id);
				});
			}
			if(isEmpty(tabs.active()) && !attr.tabsNonDefault) {
				if (tabs.tabs.length) {
					tabs.active(tabs.tabs[0].id);
				}
				else {
					tabs.firstDefault = true;
				}
			}
		},
		controller: ['$attrs', function($attrs) {
			var active = null;
			var tabNextUid = 1;
			var paneNextUid = 1;

			this.tabs = [];
			this.panes = [];
			this.firstDefault = false;

			this.active = function(id) {
				if (id == active || typeof id == 'undefined') return active;
				active = id;
				this.activate(id);
				return id;
			};

			this.activate = function(id) {
				if (typeof id == 'undefined') return;

				var i;
				if ($attrs.tabsDefault) {
					var exists = false;
					for (i = 0; i < this.tabs.length && !exists; i++) {
						exists = exists || this.tabs[i].id == id;
					}
					for (i = 0; i < this.panes.length && !exists; i++) {
						exists = exists || this.panes[i].id == id;
					}
					if (!exists) {
						id = $attrs.tabsDefault;
					}
				}

				for (i = 0; i < this.tabs.length; i++) {
					this.tabs[i][this.tabs[i].id == id ? 'open' : 'close']();
				}
				for (i = 0; i < this.panes.length; i++) {
					this.panes[i][this.panes[i].id == id ? 'open' : 'close']();
				}
			};

			this.attachTab = function(tab, isActive) {
				if (!tab.id) {
					tab.id = tabNextUid ++;
				}
				tab.classActive = tab.classActive || this.defaultClassActive;
				this.tabs.push(tab);
				var active = this.active();
				if (isActive || (this.firstDefault && isEmpty(active) && this.tabs.length == 1)) {
					this.active(tab.id);
				}
				else if (tab.id == active) {
					tab.open();
				}
			};
			this.detachTab = function(tab) {
				for (var i = 0; i < this.tabs.length; ) {
					if (this.tabs[i] === tab) {
						this.tabs.splice(i, 1);
					} else ++i;
				}
			};

			this.attachPane = function(pane, isActive) {
				if (!pane.id) {
					pane.id = paneNextUid ++;
				}
				this.panes.push(pane);
				var active = this.active();
				if (isActive || (this.firstDefault && isEmpty(active) && this.panes.length == 1)) {
					this.active(pane.id);
				}
				else if (pane.id == active) {
					pane.open();
				}
			};
			this.detachPane = function(pane) {
				for (var i = 0; i < this.panes.length; ) {
					if (this.panes[i] === pane) {
						this.panes.splice(i, 1);
					} else ++i;
				}
			};


		}]
	}
}]);


module.directive('tab', ['$animate', function($animate) {
	return {
		restrict: 'A',
		require: '^tabs',
		link: function(scope, element, attr, tabs) {

			var tab = {
				open: function() {
					if (this.opened) return;
					this.opened = true;
					if (attr.tabClassActive || this.classActive || 'ng-selected') {
						$animate.addClass(element, attr.tabClassActive || this.classActive || 'ng-selected');
					}
				},
				close: function() {
					if (!this.opened) return;
					this.opened = false;
					if (attr.tabClassActive || this.classActive || 'ng-selected') {
						$animate.removeClass(element, attr.tabClassActive || this.classActive || 'ng-selected');
					}
				},
				opened: false,
				classActive: null
			};
			tab.id = attr.tab;

			element.on('click', function() {
				scope.$apply(function() {
					tabs.active(tab.id);
				});
			});

			tabs.attachTab(tab, attr.tabActive);
			scope.$on('$destroy', function() {
				tabs.detachTab(tab);
			});
		}
	}
}]);


module.directive('tabPane', ['$animate', function($animate) {
	return {
		transclude: 'element',
		priority: 500,
		restrict: 'A',
		require: '^tabs',
		terminal: true,

		compile: function (element, attr, transclude) {
			return function (scope, element, attr, tabs) {
				var childElement, childScope;
				var pane = {
					open: function() {
						if (this.opened) return;
						this.opened = true;
						childScope = scope.$new();
						transclude(childScope, function (clone) {
							childElement = clone;
							$animate.enter(clone, element.parent(), element);
							childScope.$tabs = tabs;
						});
					},
					close: function() {
						if (!this.opened) return;
						this.opened = false;
						if (childElement) {
							$animate.leave(childElement);
							childElement = undefined;
						}
						if (childScope) {
							childScope.$destroy();
							childScope = undefined;
						}
					},
					opened: false
				};
				pane.id = attr.tabPane;
				tabs.attachPane(pane, attr.tabPaneActive);
				scope.$on('$destroy', function() {
					tabs.detachPane(pane);
				});
			}
		}
	}
}]);

module.directive('tabPaneShow', ['$animate', function($animate) {
	return {
		restrict: 'A',
		require: '^tabs',
		link: function (scope, element, attr, tabs) {
			var pane = {
				open: function() {
					if (this.opened) return;
					this.opened = true;
					$animate.removeClass(element, 'ng-hide');
				},
				close: function() {
					if (!this.opened) return;
					this.opened = false;
					$animate.addClass(element, 'ng-hide');
				},
				opened: false
			};
			element.addClass('ng-hide');
			pane.id = attr.tabPaneShow;
			tabs.attachPane(pane, attr.tabPaneActive);
			scope.$on('$destroy', function() {
				tabs.detachPane(pane);
			});
		}
	}
}]);