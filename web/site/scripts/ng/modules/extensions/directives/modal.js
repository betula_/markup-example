
module.directive('modalContainer', ['ModalManager', function(ModalManager) {
	var KEY_DEFAULT = '';
	return {
		require: 'modalContainer',
		restrict: 'AE',
		priority: 550,
		link: function(scope, element, attrs, container) {
			var name = attrs.modalContainer || attrs.name || KEY_DEFAULT;
			container.defaultBackdropName = attrs.modalBackdrop || attrs.modalBackdropDefault;
			if (attrs.modalOptionsDefault) {
				container.defaultOptions = scope.$eval(attrs.modalOptionsDefault) || container.defaultOptions;
			}
			container.name = name;
			ModalManager.containers[name] = container;
			scope.$on('$destroy', function() {
				if (ModalManager.containers[name] === container) {
					delete ModalManager.containers[name];
				}
			});
		},
		controller: 'modalContainerDirective'
	}
}]);

module.controller('modalContainerDirective', ['$animate', '$compile', '$element', '$scope', 'ModalManager', 'EventEmitterFactory', '$controller', function($animate, $compile, $element, $scope, ModalManager, EventEmitterFactory, $controller) {
	var getDashedName = function(name) {
		name && (name = name[0].toLowerCase() + name.substring(1));
		return name.replace(/([a-z0-9])([A-Z])/gm, function(match, a, b) {
			return a+'-'+b;
		}).toLowerCase();
	};

	var ModalController = function(container, backdrop, params, options) {
		EventEmitterFactory.apply(this);

		this.closed = false;
		this.container = container;
		this.backdrop = backdrop;
		this.params = params || {};
		this.options = options || {};

		this.close = function(data) {
			this.emit('close', data);
			this.closed = true;
		};
	};

	this.modals = [];
	this.defaultBackdropName = null;
	this.defaultOptions = {};

	this.modal = function(options, params, backdropName) {
		options = options || {};
		params = params || {};

		var name;
		if (typeof options == 'string') {
			name = options;
			options = {
				name: name,
				directive: name
			};
		} else {
			name = options.name || options.directive || options.templateUrl || Date.now().toString(36);
			options.name = name;
		}
		options.single = options.single || typeof options.single == 'undefined';

		var i;
		if (options.single) {
			for (i = 0; i < this.modals.length; i++) {
				if (this.modals[i].name == name) {
					return this.modals[i];
				}
			}
		}

		if (!options.directive && !options.templateUrl && options.name) {
			options.directive = options.name;
		}
		Object.keys(this.defaultOptions).forEach(function(key) {
			if (typeof options[key] == 'undefined') {
				options[key] = this.defaultOptions[key];
			}
		}.bind(this));

		options.box = options.box || options.fixed;

		var backdrop = ModalManager.getBackdrop(backdropName, this.defaultBackdropName);
		var modal = new ModalController(this, backdrop, params, options);
		modal.name = name;

		this.modals.push(modal);
		backdrop.attachModal(modal);
		var container = this;
		modal.on('close', function() {
			for (var i = 0; i < container.modals.length; ) {
				if (container.modals[i] == modal) {
					container.modals.splice(i, 1);
				} else {
					i ++;
				}
			}
		});
		modal.on('backdrop.click', function() {
			if (!options.dialog) {
				modal.close();
			}
		});
		modal.on('box.click', function() {
			modal.close();
		});

		var createModalElement = function(modalScope) {
			modalScope.$modal = modal;
			modal.$scope = modalScope;

			var builder = [];
			builder.push('<div modal=""');
			for (i in params) {
				if (params.hasOwnProperty(i)) {
					modalScope[i] = params[i];
					builder.push(getDashedName(i) + '="' + i + '"');
				}
			}
			var classBuilder = [];
			classBuilder.push('ng-modal');

			if (options.directive) {
				var _dashedDirectiveName = getDashedName(options.directive);
				builder.push(_dashedDirectiveName + '=""');
				classBuilder.push('ng-modal-'+_dashedDirectiveName);
			}

			builder.push('class="'+classBuilder.join(' ')+'"');

			if (options.templateUrl) builder.push('modal-include-template="'+options.templateUrl+'"');
			builder.push('></div>');

			var modalElement = angular.element(builder.join(' '));
			var linkModal = $compile(modalElement);

			if (options.controller) {
				var locals = options.locals || {};
				locals.$scope = modalScope;
				locals.$element = modalElement;
				var controller = $controller(options.controller, locals);
				modalElement.data('$ngControllerController', controller);
				modalElement.children().data('$ngControllerController', controller);
			}
			linkModal(modalScope);

			return modalElement;
		};

		var element = $element;
		if ($element.data('$backdropController')) {
			element = $element.data('$backdropController').getElement() || element;
		}
		if (!element.length || element[0].nodeType != Node.ELEMENT_NODE) {
			throw new Error('Cannot find parent node for modal');
		}

		if(!options.box) {
			$animate.enter(createModalElement($scope.$new()), element);
		}
		else {
			var builder = [];
			builder.push('<div modal-box=""');
			if (typeof options.box == 'string') {
				builder.push(getDashedName(options.box) + '=""');
			}
			var classBuilder = [];
			classBuilder.push('ng-modal-box');
			if (typeof options.box == 'string') {
				classBuilder.push('ng-modal-box-'+getDashedName(options.box));
			}
			if (options.directive) {
				classBuilder.push('ng-modal-'+getDashedName(options.directive)+'-box');
			}
			if (options.fixed) {
				classBuilder.push('ng-modal-box-fixed');
			}
			builder.push('class="'+classBuilder.join(' ')+'"');
			builder.push('></div>');

			var boxElement = angular.element(builder.join(' '));
			var linkBox = $compile(boxElement, function(scope, cloneFn) {
				var modalElement = createModalElement(scope);
				cloneFn(modalElement);
				return modalElement;
			});
			element[0].insertBefore(boxElement[0], null);
			linkBox($scope.$new());
		}

		return modal;
	};

}]);

module.directive('modalBackdrop', ['ModalManager', '$animate', function(ModalManager, $animate) {
	var KEY_DEFAULT = '';
	var BackdropController = function($scope, $element, $transclude) {
		var childElement, childScope;
		var closed = true;
		var closing = false;
		var self = this;

		this.modals = [];

		this.show = function() {
			if (!closed) {
				if (closing) closing = false;
				return;
			}
			closed = false;
			childScope = $scope.$new();
			$transclude(childScope, function (clone) {
				childElement = clone;
				$animate.enter(clone, $element.parent(), $element);
				childElement.on('click', function(event) {
					if (event.target == childElement[0]) {
						$scope.$apply(function() {
							for (var i = 0; i < self.modals.length; i++) {
								self.modals[i].emit('backdrop.click');
							}
						});
					}
					event.stopPropagation();
				});
			});
		};

		this.close = function(data) {
			if (closed || closing) return;
			closing = true;
			var i;
			if (this.modals.length) {
				closing = false;
				for (i = 0; i < this.modals.length; i++) {
					this.modals[i].close(data);
				}
			}
			else {
				if (closing) {
					$scope.$evalAsync(function() {
						hide();
					});
				}
			}
			function hide() {
				if (childElement) {
					$animate.leave(childElement);
					childElement = undefined;
				}
				if (childScope) {
					childScope.$destroy();
					childScope = undefined;
				}
				closed = true;
				closing = false;
			}
		};

		this.getElement = function() {
			return childElement;
		};

		this.attachModal = function(modal) {
			this.modals.push(modal);
			if (this.modals.length == 1) {
				this.show();
			}
		};

		this.detachModal = function(modal) {
			for (var i = 0; i < this.modals.length; ) {
				if (this.modals[i] === modal) {
					this.modals.splice(i, 1);
				} else ++i;
			}
			if (!this.modals.length) {
				this.close();
			}
		};
	};

	return {
		transclude: 'element',
		priority: 500,
		restrict: 'A',
		scope: true,
		compile: function (element, attrs, transclude) {
			return function(scope, element, attrs) {
				var backdrop = new BackdropController(scope, element, transclude);
				var name = attrs.modalBackdrop || KEY_DEFAULT;
				backdrop.name = name;
				ModalManager.backdrops[name] = backdrop;

				element.data('$backdropController', backdrop);

				scope.$on('$destroy', function() {
					backdrop.close();
					if (ModalManager.backdrops[name] === backdrop) {
						delete ModalManager.backdrops[name];
					}
				});
			}
		}
	}
}]);

module.factory('ModalManager', ['$controller', '$compile', function($controller, $compile) {
	var KEY_DEFAULT = '';

	return {
		containers: {},
		backdrops: {},

		getDefaultContainer: function() {
			var container = this.containers[KEY_DEFAULT];
			if (!container) {
				var body = angular.element(document.getElementsByTagName('body'));
				var scope = body.scope();
				container = $controller('modalContainerDirective', {
					$scope: scope,
					$element: body
				});
				body.data('$modalContainerController', container);
				this.containers[KEY_DEFAULT] = container;
				var self = this;
				scope.$on('$destroy', function() {
					if (self.containers[KEY_DEFAULT] === container) {
						delete self.containers[KEY_DEFAULT];
					}
				});
			}
			return container;
		},

		getContainer: function(name) {
			return this.containers[name] || this.getDefaultContainer();
		},

		getDefaultBackdrop: function() {
			var backdrop = this.backdrops[KEY_DEFAULT];
			if (!backdrop) {
				var body = angular.element(document.getElementsByTagName('body'));
				var element = $compile('<div class="ng-modal-backdrop" modal-backdrop=""></div>')(body.scope());
				body.append(element);
				backdrop = this.backdrops[KEY_DEFAULT];
				if (!backdrop) {
					throw new Error('Default backdrop can\'t be found');
				}
			}
			return backdrop;
		},

		getBackdrop: function(name) {
			var args = [].slice.call(arguments);
			for (var i = 0; i < args.length; i++) {
				if (this.backdrops[args[i]]) return this.backdrops[args[i]];
			}
			return this.getDefaultBackdrop();
		},

		show: function(options, params, containerName, backdropName) {
			return this.getContainer(containerName).modal(options, params, backdropName);
		},

		closeAll: function(data) {
			var i;
			for (i in this.backdrops) {
				if (this.backdrops.hasOwnProperty(i) && this.backdrops[i].close) {
					this.backdrops[i].close(data);
				}
			}
		},

		close: function(name, data) {
			var b = this.backdrops;
			Object.keys(b).forEach(function(key) {
				b[key].modals.forEach(function(modal) {
					if (modal && modal.name && modal.name == name) {
						modal.close(data);
					}
				});
			});
		},

		get: function(name) {
			var i, j;
			var keys = Object.keys(this.backdrops);
			var modals;
			for (i = 0; i < keys.length; i++) {
				modals = this.backdrops[keys[i]].modals;
				for (j = 0; j < modals.length; j++) {
					if (Object(modals[i]).name == name) {
						return modals[i];
					}
				}
			}
			return null;
		}
	};

}]);

module.directive('modal', ['$animate', function($animate) {
	return {
		restrict: 'A',
		require: 'modal',
		link: function(scope, element, attrs, modalCtrl) {
			var modal = modalCtrl.getModal();
			if (!modal.closed) {
				modal.once('close', function() {
					remove();
				});
			}
			else {
				remove();
			}

			scope.$on('onModalBoxInit', function(event, modalBoxCtrl) {
				modalBoxCtrl.setModal(modal);
			});

			function remove() {
				$animate.leave(element, function() {
					var modal = modalCtrl.getModal();
					modal.emit('$destroy');
				});
				var modal = modalCtrl.getModal();
				modal.backdrop.detachModal(modal);

				scope.$destroy();
				modal.$scope = undefined;
			}
		},
		controller: ['$scope', function($scope) {
			this.close = function(data) {
				this.getModal().close(data);
			};
			this.getModal = function() {
				return $scope.$modal;
			};
		}]
	}
}]);


module.directive('modalIncludeTemplate', [function() {
	return {
		restrict: 'A',
		require: 'modal',
		replace: true,
		templateUrl: function(element, attrs) {
			return attrs.modalIncludeTemplate;
		}
	}
}]);

module.directive('modalTransclude', ['$animate', function($animate) {
	return {
		controller: ['$transclude', function($transclude) {
			this.$transclude = $transclude;
		}],
		link: function($scope, $element, $attrs, ctrl) {
			ctrl.$transclude(function(clone) {
				$animate.enter(clone, $element);
			});
		}
	}
}]);

module.directive('modalBox', [function() {
	return {
		restrict: 'A',
		require: 'modalBox',

		template: '<div class="ng-modal-box-container" modal-transclude=""></div>',

		link: function($scope, $element, $attrs, ctrl) {
			$element.on('click', function(event) {
				var element = angular.element(event.target);
				var clickIn = false;
				while (element.length && !clickIn) {
					if (element.data('$modalController')) {
						clickIn = element.data('$modalController').getModal() === ctrl.getModal();
					}
					element = element.parent();
				}
				if (!clickIn) {
					var modal = ctrl.getModal();
					if (modal && !modal.closed) {
						$scope.$apply(function() {
							modal.emit('box.click');
						});
					}
				}
				event.stopPropagation();
			});
			$scope.$broadcast('onModalBoxInit', ctrl);
		},
		controller: ['$scope', '$element', function($scope, $element) {
			this.modal = null;
			this.getModal = function() {
				return this.modal;
			};
			this.setModal = function(modal) {
				if (!modal.closed) {
					this.modal = modal;
					modal.once('$destroy', function() {
						$scope.$evalAsync(function() {
							remove();
						})
					});
				}
				else {
					remove();
				}

				function remove() {
					$element.remove();
					$scope.$destroy();
				}
			};
		}]
	}
}]);

module.directive('scrollableUnderFixedModal', ['ModalManager', function(ModalManager) {
	return {
		restrict: 'A',
		link: function(scope, element) {
			var classAdded = false;
			scope.$watch(function() {
				var i, j;
				var keys = Object.keys(ModalManager.backdrops);
				var modals;
				for (i = 0; i < keys.length; i++) {
					modals = ModalManager.backdrops[keys[i]].modals;
					for (j = 0; j < modals.length; j++) {
						if (Object(modals[i]).options.fixed) {
							return true;
						}
					}
				}
				return false;
			}, function(val) {
				if (val && !classAdded) {
					element.addClass('ng-scroll-off');
					element.css({
						overflow: 'hidden'
					});
					classAdded = true;
				}
				else if (!val && classAdded) {
					element.removeClass('ng-scroll-off');
					element.css({
						overflow: 'auto'
					});
					classAdded = false;
				}
			});
		}
	}
}]);

module.directive('clickModalShow', ['ModalManager', function(ModalManager) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			element.on('click', function() {
				scope.$apply(function() {
					var params = {};
					if (attrs.modalParams) {
						params = scope.$eval(attrs.modalParams);
					}
					var options = {};
					if (attrs.modalOptions) {
						options = scope.$eval(attrs.modalOptions);
					}
					options.name = attrs.clickModalShow;
					if (attrs.modalTemplateUrl) {
						options.templateUrl = attrs.modalTemplateUrl;
					}
					ModalManager.show(options, params, attrs.modalShowContainer, attrs.modalShowBackdrop);
				});
			});
		}
	}
}]);

module.directive('clickModalClose', [function() {
	return {
		require: '^modal',
		link: function(scope, element, attrs, modal) {
			element.on('click', function(e) {
				scope.$apply(function() {
					var defaultPrevented = false;
					if (attrs.clickModalClose) {
						scope.$eval(attrs.clickModalClose, {
							$event: { preventDefault: function() { defaultPrevented = true; } }, $clickEvent: e
						});
					}
					if (!defaultPrevented) modal.close();
				});
			});
		}
	}
}]);
