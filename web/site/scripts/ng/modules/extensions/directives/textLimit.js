
module.directive('textLimit', [function() {
	function isEmpty(value) {
		return typeof value == 'undefined' || value === '' || value === null || value !== value;
	}
	return {
		require: 'ngModel',
		restrict: 'A',
		link: function(scope, element, attrs, ctrl) {
			attrs.$set('maxlength', attrs.textLimit);
			ctrl.$parsers.unshift(function(viewValue) {
				if (isEmpty(viewValue) || typeof viewValue != 'string' || !attrs.textLimit || viewValue.length <= attrs.textLimit) {
					return viewValue;
				}
				viewValue = viewValue.substring(0, attrs.textLimit);
				ctrl.$viewValue = viewValue;
				ctrl.$render();
				return viewValue;
			});
		}
	}
}]);