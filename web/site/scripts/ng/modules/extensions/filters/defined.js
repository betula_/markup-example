module.filter('defined', function() {
	return function(input) {
		return typeof input !== 'undefined';
	}
});