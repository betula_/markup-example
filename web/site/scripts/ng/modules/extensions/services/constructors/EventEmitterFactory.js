
module.factory('EventEmitterFactory', [function() {
	return function(object) {
		object = object || (this !== window ? this : {});
		object.listeners = {};
		object.emit = function(name, data) {
			var args = Array.prototype.slice.call(arguments, 1);
			if (!this.listeners[name]) return;
			var listeners = this.listeners[name].slice();
			for (var i = 0; i < listeners.length; i++) {
				listeners[i].apply(object, args);
			}
		};
		object.on = function(name, fn) {
			var emitter = this;
			var names = name.split(/\s+/).filter(function(v) { return v; });
			if (names.length > 1) {
				var offs = names.map(function(name) {
					return emitter.on(name, fn);
				});
				return function() {
					offs.forEach(function(off) {
						off();
					});
				}
			}
			name = names[0];
			if (!this.listeners[name]) this.listeners[name] = [];
			this.listeners[name].push(fn);

			return function() {
				emitter.off(name, fn);
			};
		};
		object.off = function(name, fn) {
			var emitter = this;
			var names = name.split(/\s+/).filter(function(v) { return v; });
			if (names.length > 1) {
				names.forEach(function(name) {
					emitter.off(name, fn);
				});
				return;
			}
			name = names[0];
			if (!this.listeners[name]) return;
			for (var i = 0; i < this.listeners[name].length; ) {
				if (this.listeners[name][i] === fn) {
					this.listeners[name].splice(i, 1);
				} else ++i;
			}
		};
		object.once = function(name, fn) {
			var off = this.on(name, function() {
				fn.apply(this, arguments);
				off();
			});
			return off;
		};
		return object;
	}
}]);