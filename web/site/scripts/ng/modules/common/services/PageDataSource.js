
module.service('PageDataSource', ['$window', function($window) {

	var data = $window.__pageDataSource || {};
	this.getData = function(key) {
		if (typeof key == 'undefined') {
			return data;
		}
		return data[key] = data[key] || {};
	}

}]);