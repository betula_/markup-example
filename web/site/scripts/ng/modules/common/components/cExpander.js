module.directive('cExpander', ['jQuery', function(jQuery) {
	return {
		restrict: 'EA',
		transclude: true,
		scope: true,
		templateUrl: 'template:c-expander.html',

		link: function(scope, element, attrs) {

			var box = element.find('.c-expander:eq(0)');
			var expander = element.find('.p-expander-content:eq(0)');

			scope._expand = function() {
				box.css({
					maxHeight: expander.height()
				});
				box.addClass('ng-expanded');
				box.removeClass('ng-collapsed');
			};
			scope._collapse = function() {
				box.addClass('ng-collapsed');
				box.removeClass('ng-expanded');
				box.css({
					maxHeight: ''
				});
			};

			refresh();
			setTimeout(refresh, 0);
			var tid = setInterval(refresh, 300);

			scope.$on('$destroy', function() {
				clearInterval(tid);
			});

			function refresh() {
				if (box.hasClass('ng-expandable')) {
					return;
				}
				if (box.height() < expander.height()) {
					clearInterval(tid);
					box.addClass('ng-expandable ng-collapsed');
				}
			}

		}
	}
}]);