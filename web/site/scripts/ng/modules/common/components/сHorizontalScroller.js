module.directive('cHorizontalScroller', [function() {
	return {
		restrict: 'EA',
		transclude: true,
		templateUrl: 'template:c-horizontal-scroller.html'
	}
}]);