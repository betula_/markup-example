
var module = angular.module('mif.modules.common', ['mif.modules.extensions', 'ui.router', 'ngSanitize', 'angulartics', 'angulartics.google.analytics']);

module.config(['$locationProvider', '$sceProvider', function($locationProvider, $sceProvider) {
	$locationProvider.html5Mode(false);
	$sceProvider.enabled(true);
}]);

module.run(['$injector', function($injector) {
	// Run page
	$injector.get('Page');
}]);