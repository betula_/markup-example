
module.directive('scroller', ['jQuery', 'IScroll', 'EventEmitterFactory', function(jQuery, IScroll, EventEmitterFactory) {
	return {
		restrict: 'AE',
		scope: true,
		require: 'scroller',
		controller: ['$scope', '$element', '$attrs', function($scope, $element, $attrs) {
			EventEmitterFactory.apply(this);

			var options = {};

			if (typeof $attrs.horizontalSnap != 'undefined') {
				options = {
					scrollX: true,
					scrollY: false,
					eventPassthrough: true,
					preventDefault: false,
					momentum: false,
					snap: $attrs.horizontalSnap ? $attrs.horizontalSnap : true,
					keyBindings: true
				};
			}
			if ($attrs.scroller) {
				var opts = $scope.$eval($attrs.scroller);
				if (opts) {
					Object.keys(opts).forEach(function(key) {
						options[key] = opts[key];
					});
				}
			}

			this.element = $element;

			this.init = function() {
				var scroll = this.scroll = new IScroll(this.element[0], options);
				var self = this;

				var container = this.element.children()[0];
				var prevWidth = container.clientWidth;

				this.refresh = function() {
					var width = container.clientWidth;
					if (width === prevWidth) return;
					prevWidth = width;
					scroll.refresh();
				};

				this.next = function() {
					self.scroll.next();
				};
				this.prev = function() {
					self.scroll.prev();
				};

				var unwatch = (function() {
					var handler = setInterval(function() {
						self.refresh();
					}, 500);
					return function() {
						clearInterval(handler);
					}
				})();

				$scope.$on('$destroy', function() {
					unwatch();
					scroll.destroy();
					self.scroll = scroll = null;
				});

				scroll.on('scrollStart', function() {
					self.emit('scrollStart');
				});
				scroll.on('scrollEnd', function() {
					self.emit('scrollEnd');
				});
				scroll.on('beforeScrollStart', function() {
					self.emit('beforeScrollStart');
				});
				scroll.on('scrollCancel', function() {
					self.emit('scrollCancel');
				});
				scroll.on('refresh', function() {
					self.emit('refresh');
				});

				self.emit('init');
			};

			if (this.element.find('[scroller-element]').length) {
				setTimeout(this.init.bind(this));
			}
			else {
				this.init();
			}

		}],
		compile: function() {
			return {
				pre: function(scope, element, attrs, scroller) {
					scope.$scroller = scroller;
				}
			}
		}
	}
}]);

module.directive('scrollerNext', [function() {
	return {
		restrict: 'A',
		require: '^scroller',
		link: function(scope, element, attrs, scroller) {
			var unbind = scroller.on('init refresh scrollStart scrollEnd beforeScrollStart scrollCancel', function() {
				var scroll = scroller.scroll;
				if (scroll.maxScrollX) {
					if (scroll.x == scroll.maxScrollX) {
						element.addClass('ng-hide');
					} else {
						element.removeClass('ng-hide');
					}
				}
				else {
					element.addClass('ng-hide');
				}
				element.addClass('ng-initialized');
			});
			scope.$on('$destroy', unbind);

			element.on('click', function() {
				scroller.next();
			});
		}
	}
}]);

module.directive('scrollerPrev', [function() {
	return {
		restrict: 'A',
		require: '^scroller',
		link: function(scope, element, attrs, scroller) {
			var unbind = scroller.on('init refresh scrollStart scrollEnd beforeScrollStart scrollCancel', function() {
				var scroll = scroller.scroll;
				if (scroll.maxScrollX) {
					if (!scroll.x) {
						element.addClass('ng-hide');
					} else {
						element.removeClass('ng-hide');
					}
				}
				else {
					element.addClass('ng-hide');
				}
				element.addClass('ng-initialized');
			});
			scope.$on('$destroy', unbind);


			element.on('click', function() {
				scroller.prev();
			});
		}
	}
}]);

module.directive('scrollerElement', [function() {
	return {
		restrict: 'A',
		require: '^scroller',
		link: function(scope, element, attrs, scroller) {
			scroller.element = element;
		}
	}
}]);

module.directive('scrollerDragBlocker', [function() {
	return {
		restrict: 'A',
		require: '^scroller',
		link: function(scope, element, attrs, scroller) {
			var unbindStart = scroller.on('scrollStart', function() {
				element.addClass('ng-show');
			});
			var unbindEnd = scroller.on('scrollEnd scrollCancel refresh', function() {
				element.removeClass('ng-show');
			});

			element.on('mouseup', function() {
				element.removeClass('ng-show');
			});

			scope.$on('$destroy', function() {
				unbindStart();
				unbindEnd();
			});
		}
	}
}]);
