module.directive('uiStateIs', ['$animate', '$state', function($animate, $state) {
	return {
		transclude: 'element',
		priority: 500,
		restrict: 'A',
		terminal: true,

		compile: function (element, attrs, transclude) {
			return function (scope, element, attrs) {
				var childElement, childScope;
				var ctrl = {
					open: function() {
						if (this.opened) return;
						this.opened = true;
						childScope = scope.$new();
						transclude(childScope, function (clone) {
							childElement = clone;
							$animate.enter(clone, element.parent(), element);
						});
					},
					close: function() {
						if (!this.opened) return;
						this.opened = false;
						if (childElement) {
							$animate.leave(childElement);
							childElement = undefined;
						}
						if (childScope) {
							childScope.$destroy();
							childScope = undefined;
						}
					},
					opened: false
				};

				scope.$watch(function() {
					return $state.is(attrs.uiStateIs);
				}, function(value) {
					ctrl[ value ? 'open' : 'close' ]();
				});

			}
		}
	}
}]);