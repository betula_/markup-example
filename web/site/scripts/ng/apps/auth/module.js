
var module = angular.module('mif.apps.auth', ['mif.modules.common']);

module.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider
		.otherwise('/login');

	$stateProvider
		.state('index', {
			url: '/',
			onEnter: ['Page', function(Page) {
				Page.routeIndexAction();
			}]
		})
		.state('root', {
			url: '',
			onEnter: ['Page', function(Page) {
				Page.routeIndexAction();
			}]
		})
		.state('index.login', {
			url: 'login',
			onEnter: ['Page', function(Page) {
				Page.routeStateAction('login');
			}]
		})
		.state('index.registration', {
			url: 'registration',
			onEnter: ['Page', function(Page) {
				Page.routeStateAction('registration');
			}]
		})
		.state('index.activate', {
			url: 'activate?token',
			resolve: {
				tokenInfo: ['ActivationToken', '$stateParams', function(ActivationToken, $stateParams) {
					return ActivationToken.getInfo($stateParams.token);
				}]
			},
			onEnter: ['$stateParams', 'Page', 'tokenInfo', function($stateParams, Page, tokenInfo) {
				Page.routeActivateAction($stateParams.token, tokenInfo);
			}]
		})
		.state('index.recover', {
			url: 'password/recover',
			onEnter: ['Page', function(Page) {
				Page.routeStateAction('recover');
			}]
		})
		.state('index.change', {
			url: 'password/change?token',
			resolve: {
				tokenInfo: ['ChangeToken', '$stateParams', function(ChangeToken, $stateParams) {
					return ChangeToken.getInfo($stateParams.token);
				}]
			},
			onEnter: ['$stateParams', 'Page', 'tokenInfo', function($stateParams, Page, tokenInfo) {
				Page.routeChangeAction($stateParams.token, tokenInfo);
			}]
		})
	;
}]);

module.run(['$rootScope', 'Page', function($rootScope, Page) {
	$rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
		if (fromState.name) {
			Page.changeStateError(toState.name, toParams, fromState.name, fromParams, error);
			return;
		}
		Page.startStateError(toState.name, toParams, error);
	});
}]);

module.run(['$injector', function($injector) {
	// Run ui-router
	$injector.get('$state');
}]);
