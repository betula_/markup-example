
module.controller('AuthActivate', ['AuthActivate', 'Page', '$scope', '$sessionStorageStore', function(AuthActivate, Page, $scope, $sessionStorageStore) {

	var self = this;
	this.state = AuthActivate;
	this.local = {};
	this.hasRequestError = false;

	$scope.$watch(function() {
		return self.state.user.name;
	}, function(name) {
		self.local.name = name;
	});
	$scope.$watch(function() {
		return self.state.user.surname;
	}, function(surname) {
		self.local.surname = surname;
	});

	this.success = function() {
		$sessionStorageStore.put('email', this.state.user.email);
		this.hasRequestError = false;
		Page.navigateToPrivate();
	};

	this.error = function(response) {
		response = response || {};
		this.hasRequestError = response.status > 400;
	};

}]);