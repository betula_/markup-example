
module.controller('AuthPanel', ['AuthPanel', function(AuthPanel) {

	this.state = AuthPanel;

	this.isAuth = function() {
		return ['login', 'registration'].indexOf(this.state.active) != -1;
	};
	this.isActivate = function() {
		return this.state.active == 'activate';
	};
	this.isRecover = function() {
		return this.state.active == 'recover';
	};
	this.isChange = function() {
		return this.state.active == 'change';
	};

}]);