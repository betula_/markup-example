
module.controller('AuthLogin', ['Page', '$sessionStorageStore', '$scope', function(Page, $sessionStorageStore, $scope) {

	var self = this;
	this.local = {};
	this.hasRequestError = false;
	this.hasDenied = false;

	$scope.$watch(function() {
		return $sessionStorageStore.get('email');
	}, function(email) {
		if (email) {
			self.local.email = email;
		}
	});

	$scope.$watch(function() {
		return self.local.email
	}, function(email, prev) {
		if (email == prev) {
			return;
		}
		$sessionStorageStore.put('email', email);
	});

	this.success = function() {
		Page.navigateToPrivate();
		this.hasRequestError = false;
		this.hasDenied = false;
	};

	this.error = function(response, form) {
		response = response || {};
		this.hasRequestError = response.status > 400 && response.status != 404;
		this.hasDenied = (form && (form.email.$error.not_found || form.password.$error.invalid));
	};

	this.transform = function(data) {
		data = data || {};
		data.email = String(data.email).toLowerCase();
		this.local.email = data.email;
		return data;
	};

}]);