
module.controller('AuthRegistration', ['Page', '$sessionStorageStore', '$scope', function(Page, $sessionStorageStore, $scope) {

	var self = this;
	this.hasFinishMessage = false;
	this.local = {};
	this.hasRequestError = false;

	$scope.$watch(function() {
		return $sessionStorageStore.get('email');
	}, function(email) {
		if (email) {
			self.local.email = email;
		}
	});

	$scope.$watch(function() {
		return self.local.email
	}, function(email, prev) {
		if (email == prev) {
			return;
		}
		$sessionStorageStore.put('email', email);
	});

	this.success = function(data) {
		data = data || {};
		switch (data.status) {
			case 'active':
				self.local.email = data.email;
				Page.navigateToLogin();
				break;
			case 'inactive':
			default:
				this.hasFinishMessage = true;
		}
		this.hasRequestError = false;
	};

	this.error = function(response) {
		response = response || {};
		this.hasRequestError = response.status > 400;
	};

	this.transform = function(data) {
		data = data || {};
		data.email = String(data.email).toLowerCase();
		this.local.email = data.email;
		return data;
	};

}]);