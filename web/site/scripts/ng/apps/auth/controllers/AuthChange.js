
module.controller('AuthChange', ['AuthChange', 'Page', '$sessionStorageStore', function(AuthChange, Page, $sessionStorageStore) {

	var self = this;
	this.state = AuthChange;
	this.local = {};
	this.hasRequestError = false;

	this.success = function() {
		$sessionStorageStore.put('email', this.state.user.email);
		this.hasRequestError = false;
		Page.navigateToPrivate();
	};

	this.error = function(response) {
		response = response || {};
		this.hasRequestError = response.status > 400;
	};

}]);