
module.factory('ActivationToken', ['$http', function($http) {

	var self = {};

	self.getInfo = function(token) {
		var params = {
			token: token
		};
		var promise = $http.get('/auth/activate.ajax', {
			params: params
		});
		return promise.then(function(response) {
			return response.data;
		});
	};

	return self;
}]);