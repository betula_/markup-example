module.config(['$provide', function($provide) {
	$provide.decorator('Page', ['$delegate', 'AuthPanel', '$rootScope', '$state', '$window', '$location', 'AuthActivate', 'AuthChange', function($delegate, AuthPanel, $rootScope, $state, $window, $location, AuthActivate, AuthChange) {

		var copy = angular.copy;
		var self = $delegate;

		self.routeIndexAction = function() {
		};

		self.routeStateAction = function(state) {
			AuthPanel.active = state;
		};

		self.routeActivateAction = function(token, tokenInfo) {
			AuthPanel.active = 'activate';
			AuthActivate.token = token;
			copy(tokenInfo.user, AuthActivate.user);
			copy(tokenInfo.library, AuthActivate.library);
		};

		self.routeChangeAction = function(token, tokenInfo) {
			AuthPanel.active = 'change';
			AuthChange.token = token;
			copy(tokenInfo.user, AuthChange.user);
		};

		self.navigateToPrivate = function() {
			$window.location = '/';
		};

		self.navigateToLogin = function() {
			$window.location = '/auth/#/login';
		};


		self.changeStateError = function() {
		};

		self.startStateError = function(name) {
			if (name == 'index.activate') {
				$state.go('index');
			}
		};

		self.changeActivePanel = function(state) {
			if ($state.is('index.'+state) !== true) {
				$state.go('index.'+state);
			}
		};

		$rootScope.$watch(function() {
			return AuthPanel.active;
		}, function(state, prev) {
			if (state && state != prev) {
				self.changeActivePanel(state);
			}
		});



		return self;

	}]);
}]);