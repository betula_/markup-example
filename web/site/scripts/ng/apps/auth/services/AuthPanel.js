
module.service('AuthPanel', [function() {

	var self = this;

	this._active = 'login';
	this.activeAllowList = ['login', 'registration', 'activate', 'recover', 'change'];

	Object.defineProperty(this, 'active', {
		get: function() {
			return self._active;
		},
		set: function(value) {
			if (self.activeAllowList.indexOf(value) != -1) {
				self._active = value;
			}
			else if(!self._active) {
				self._active = self.activeAllowList[0];
			}
		}
	});


}]);