module.directive('cMainSearchForm', [function() {
	return {
		restrict: 'EA',
		scope: true,
		templateUrl: 'template:c-main-search-form.html',
		controller: 'MainSearchForm as search'
	}
}]);