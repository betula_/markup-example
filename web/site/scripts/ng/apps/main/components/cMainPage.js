module.directive('cMainPage', ['LibrarySettings', function(LibrarySettings) {
	return {
		restrict: 'EA',
		scope: true,
		templateUrl: 'template:c-main-page.html',
		link: function(scope, element, attrs) {
			scope.settigns = LibrarySettings;
		}
	}
}]);