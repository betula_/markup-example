module.directive('cMainBookTile', [function() {
	return {
		restrict: 'EA',
		scope: {
			book: '=cMainBookTile',
			isFirstBookInList: '=?firstInList'
		},
		templateUrl: 'template:c-main-book/tile.html'
	}
}]);