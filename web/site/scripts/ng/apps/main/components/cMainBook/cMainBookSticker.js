module.directive('cMainBookSticker', [function() {
	return {
		restrict: 'EA',
		require: '^cMainBookStickers',
		link: function(scope, element, attrs, stickers) {
			element.addClass(stickers.colors.shift());
		}
	}
}]);
