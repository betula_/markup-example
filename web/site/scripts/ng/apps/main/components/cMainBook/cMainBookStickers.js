module.directive('cMainBookStickers', [function() {
	return {
		restrict: 'EA',
		controller: [function() {
			this.colors = ['m-pink', 'm-purple', 'm-white', 'm-green', 'm-yellow', 'm-orange', 'm-black', 'm-blue'].sort(function(a, b) {
				return Math.random() - .5;
			});
		}]
	}
}]);
