module.directive('cMainBookPhoto', [function() {
	return {
		restrict: 'EA',
		scope: {
			photo: '=cMainBookPhoto'
		},
		templateUrl: 'template:c-main-book/photo.html'
	}
}]);
