module.directive('cMainBookQuote', [function() {
	return {
		restrict: 'EA',
		scope: {
			quote: '=cMainBookQuote'
		},
		templateUrl: 'template:c-main-book/quote.html'
	}
}]);
