module.directive('cMainBookExpert', [function() {
	return {
		restrict: 'EA',
		scope: {
			expert: '=cMainBookExpert'
		},
		templateUrl: 'template:c-main-book/expert.html'
	}
}]);
