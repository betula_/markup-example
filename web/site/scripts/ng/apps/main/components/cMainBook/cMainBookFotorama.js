module.directive('cMainBookFotorama', [function() {
	return {
		restrict: 'EA',
		link: function(scope, element) {
			setTimeout(function() {
				element.addClass('fotorama').fotorama({
					'nav': false
				});
			});
		}
	}
}]);
