module.directive('cMainPageGreeting', ['LibrarySettings', function(LibrarySettings) {
	return {
		restrict: 'EA',
		scope: true,
		templateUrl: 'template:c-main-page-greeting.html',
		link: function(scope, element, attrs) {
			scope.greeting = LibrarySettings.greeting;
		}
	}
}]);