
var module = angular.module('mif.apps.main', ['mif.modules.common']);

module.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider
		.otherwise('/');

	$stateProvider
		.state('home', {
			url: '/',
			templateUrl: 'template:c-main-home.html',
			resolve: {
				shelves: ['BooksShelves', function(BooksShelves) {
					BooksShelves.params.page = 'main';
					return BooksShelves.perform();
				}]
			},
			controller: 'MainHome as model'
		})
		.state('books', {
			url: '/books',
			templateUrl: 'template:c-main-books.html',
			resolve: {
				books: ['BooksList', function(BooksList) {
					return BooksList.perform();
				}]
			},
			controller: 'MainBooks as model'
		})
		.state('shelves', {
			url: '/shelves',
			templateUrl: 'template:c-main-shelves.html',
			resolve: {
				shelves: ['BooksShelves', function(BooksShelves) {
					BooksShelves.params.page = 'list';
					return BooksShelves.perform();
				}]
			},
			controller: 'MainShelves as model'
		})
		.state('top', {
			url: '/top',
			templateUrl: 'template:c-main-top.html',
			resolve: {
				books: ['BooksTop', function(BooksTop) {
					return BooksTop.perform();
				}]
			},
			controller: 'MainTop as model'
		})
		.state('book', {
			url: '/book/{id}',
			templateUrl: 'template:c-main-book.html',
			resolve: {
				book: ['Book', '$stateParams', function(Book, $stateParams) {
					return Book.perform($stateParams.id);
				}]
			},
			controller: 'MainBook as model'
		})
	;
}]);