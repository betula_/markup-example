
module.controller('MainSearchForm', ['$scope', '$timeout', '$http', '$rootScope', '$q', '$location', function($scope, $timeout, $http, $rootScope, $q, $location) {
	var DELAY = 334;
	var URL = '/books/search.ajax';
	var SUGGESTS_LIMIT = 10000;

	var copy = angular.copy;
	var self = this;

	this.query = '';

	this.go = function(id) {
		if (!id) {
			return;
		}
		//self.query = '';
		self.request.cancel();
		self.suggests.hide();

		$location.path('/book/'+id);
	};

	this.submit = function(event) {
		event.preventDefault();

		var active = this.suggests.items.filter(function(i) {
			return i.$active;
		})[0];
		if (active) {
			this.go(active.id);
		}
		else if (this.suggests.items.length == 1) {
			this.go(this.suggests.items[0].id);
		}

	};

	this.focus = function() {
		this.request(true);
	};

	this.keyDown = function(event) {
		if (event.keyCode == 13) {
			return;
		}
		if (event.keyCode == 40) { // Arrow down
			if (this.suggests.isVisible) {
				this.suggests.selectNext();
			}
			else {
				this.request();
			}
			event.preventDefault();
			return;
		}
		if (event.keyCode == 38) { // Arrow up
			if (this.suggests.isVisible) {
				this.suggests.selectPrev();
			}
			else {
				this.request();
			}
			event.preventDefault();
			return;
		}

		$timeout(function() {
			self.request(); // Next tick
		}, 0);
	};

	this.suggests = {
		items: [],
		setItems: function(items) {
			var suggests = this;
			this.items.length = 0;
			items.forEach(function(item) {
				var obj = copy(item);
				obj.title = (obj.title || '').replace(/<\/?[^>]+>/gi, '');
				obj.$click = function() {

					self.go(this.id);
				};
				obj.$enter = function() {
					suggests.items.forEach(function(item) {
						if (item != obj) {
							item.$active = false;
						}
					});
					obj.$active = true;
				};
				obj.$leave = function() {
					obj.$active = false;
				};
				suggests.items.push(obj);
			});
			this[items.length ? 'show' : 'hide']();
		},
		selectItem: function(item) {
			if (!isNaN(item)) {
				item = this.items[item];
			}
			this.items.forEach(function(i) {
				if (i != item) {
					i.$active = false;
				}
			});
			if (item) {
				item.$active = true;
				self.query = item.title;
			}
			else {
				self.query = self.lastQuery || '';
			}
		},
		selectNext: function() {
			var i;
			var indexOf = -1;
			for (i = 0; i < this.items.length; i++) {
				if (this.items[i].$active) {
					indexOf = i;
					break;
				}
			}
			if (indexOf >= 0) {
				this.selectItem(indexOf+1);
			}
			else {
				this.selectItem(0);
			}
		},
		selectPrev: function() {
			var i;
			var indexOf = -1;
			for (i = 0; i < this.items.length; i++) {
				if (this.items[i].$active) {
					indexOf = i;
					break;
				}
			}
			if (indexOf >= 0) {
				this.selectItem(indexOf-1);
			}
			else {
				this.selectItem(this.items.length-1);
			}
		},

		isVisible: false,
		hide: function() {
			this.isVisible = false;
		},
		show: function() {
			this.isVisible = true;
		}
	};

	var timer;
	var promise;
	var canceler;
	var lastRequest;
	this.lastQuery = null;
	this.pending = false;
	this.request = function(force) {
		this.lastQuery = this.query;
		var self = this;
		if (promise) {
			promise.finally(function() {
				self.request();
			});
			return;
		}
		$timeout.cancel(timer);
		timer = $timeout(perform, DELAY);

		if (force && !promise) {
			perform();
		}

		function perform() {
			if (lastRequest == self.query) {
				self.suggests[self.suggests.items.length ? 'show' : 'hide']();
				return;
			}
			if (!self.query) {
				lastRequest = self.query;
				self.suggests.hide();
				return;
			}
			var params = {
				q: self.query,
				limit: SUGGESTS_LIMIT
			};
			lastRequest = params.query;
			self.pending = true;
			canceler = $q.defer();
			promise = $http.get(URL, {
				params: params,
				timeout: canceler.promise,
				cache: true
			});
			promise.finally(function() {
				self.pending = false;
				promise = null;
			});
			promise.then(function(response) {
				var items = ((response.data || {}).books || []).slice(0, SUGGESTS_LIMIT);
				self.suggests.setItems(items);
			}, function() {
				lastRequest = null;
			});
		}
	};

	this.request.cancel = function() {
		$timeout.cancel(timer);
		if (canceler) {
			canceler.resolve();
		}
	};

}]);