
module.factory('Book', ['$http', function($http) {

	var self = {};

	self.params = {
		id: null
	};

	self.queryPromise = null;
	self.query = function() {
		if (this.queryPromise) {
			return this.queryPromise;
		}
		var self = this;
		var promise = $http.get('/books/' + self.params.id + '.ajax', {
			cache: true
		});
		this.queryPromise = promise;
		promise.finally(function() {
			self.queryPromise = null;
		});
		return promise;
	};

	self.perform = function(id) {
		if (typeof id != 'undefined') {
			self.params.id = id;
		}
		return this.update();
	};

	self.updatePromise = null;
	self.update = function() {
		if (this.updatePromise) {
			return this.updatePromise;
		}
		var self = this;
		var promise = this.query().then(function(response) {
			var book = response.data || [];

			if (self.$keys) {
				self.$keys.forEach(function(key) {
					delete self[key];
				});
			}

			var keys = Object.keys(book);
			keys.forEach(function(key) {
				self[key] = book[key];
			});
			self.$keys = keys;

			return self;
		});
		this.updatePromise = promise;
		promise.finally(function() {
			self.updatePromise = null;
		});
		return promise;
	};


	return self;
}]);