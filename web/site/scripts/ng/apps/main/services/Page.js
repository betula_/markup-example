module.config(['$provide', function($provide) {
	$provide.decorator('Page', ['$delegate', '$rootScope', '$state', '$location', function($delegate, $rootScope, $state, $location) {

		var page = $delegate;

		page.eventDoSearchQueryListener = function(event, data) {
			page.eventDoSearchQueryAction(event, data);
		};
		$rootScope.$on('doSearchQuery', page.eventDoSearchQueryListener.bind(page));

		page.eventDoSearchQueryAction = function(event, data) {
			if (!$state.is('search')) {
				$state.go('search', {
					query: data.query
				});
			}
			else {
				$location.search('query', data.query);
			}
		};

		return page;

	}]);
}]);