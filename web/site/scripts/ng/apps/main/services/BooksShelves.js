
module.factory('BooksShelves', ['$http', function($http) {
	var LIMIT = 10000;

	var copy = angular.copy;
	var self = [];

	self.params = {
		page: 'list'
	};

	self.queryPromise = null;
	self.query = function() {
		if (this.queryPromise) {
			return this.queryPromise;
		}
		var self = this;
		var params = {
			limit: LIMIT,
			page: 'list'
		};
		if (['main'].indexOf(self.params.page) >= 0) {
			params.page = self.params.page;
		}

		var promise = $http.get('/books/shelves.ajax', {
			params: params,
			cache: true
		});
		this.queryPromise = promise;
		promise.finally(function() {
			self.queryPromise = null;
		});
		return promise;
	};

	self.perform = function() {
		return this.update();
	};

	self.updatePromise = null;
	self.update = function() {
		if (this.updatePromise) {
			return this.updatePromise;
		}
		var self = this;
		var promise = this.query().then(function(response) {
			self.length = 0;
			var shelves = response.data || [];
			shelves.forEach(function(shelf) {
				self.push(copy(shelf));
			});
			return self;
		});
		this.updatePromise = promise;
		promise.finally(function() {
			self.updatePromise = null;
		});
		return promise;
	};


	return self;
}]);