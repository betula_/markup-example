
module.factory('BooksTop', ['$http', function($http) {
	var LIMIT = 10000;

	var self = [];

	self.params = {
		interval: 'all'
	};

	self.queryPromise = null;
	self.query = function() {
		if (this.queryPromise) {
			return this.queryPromise;
		}
		var self = this;
		var params = {
			limit: LIMIT,
			from: 0
		};
		var dt;
		if (['month', 'm'].indexOf(self.params.interval) >= 0) {
			dt = new Date();
			dt.setDate(1);
			params.from = parseInt(dt.getTime() / 1000);
		}

		var promise = $http.get('/books/stats.ajax', {
			params: params,
			cache: true
		});
		this.queryPromise = promise;
		promise.finally(function() {
			self.queryPromise = null;
		});
		return promise;
	};

	self.perform = function() {
		return this.update();
	};

	self.updatePromise = null;
	self.update = function() {
		if (this.updatePromise) {
			return this.updatePromise;
		}
		var self = this;
		var promise = this.query().then(function(response) {
			self.length = 0;
			var list = response.data || [];
			list.forEach(function(item) {
				var book = item.book;
				book.downloads = item.downloads;
				self.push(book);
			});
			return self;
		});
		this.updatePromise = promise;
		promise.finally(function() {
			self.updatePromise = null;
		});
		return promise;
	};


	return self;
}]);