
module.factory('BooksList', ['$http', function($http) {
	var LIMIT = 10000;

	var self = [];

	self.params = {
		type: 'all'
	};

	self.queryPromise = null;
	self.query = function() {
		if (this.queryPromise) {
			return this.queryPromise;
		}
		var self = this;
		var params = {
			limit: LIMIT
		};
		if (['audiobook', 'ebook'].indexOf(self.params.type) >= 0) {
			params.type = self.params.type;
		}

		var promise = $http.get('/books/list.ajax', {
			params: params,
			cache: true
		});
		this.queryPromise = promise;
		promise.finally(function() {
			self.queryPromise = null;
		});
		return promise;
	};

	self.perform = function() {
		return this.update();
	};

	self.updatePromise = null;
	self.update = function() {
		if (this.updatePromise) {
			return this.updatePromise;
		}
		var self = this;
		var promise = this.query().then(function(response) {
			self.length = 0;
			var books = (response.data || {}).books || [];
			books.forEach(function(book) {
				self.push(book);
			});
			return self;
		});
		this.updatePromise = promise;
		promise.finally(function() {
			self.updatePromise = null;
		});
		return promise;
	};


	return self;
}]);