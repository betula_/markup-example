var es = require('event-stream');
var path = require('path');
var gutil = require('gulp-util');
var concat = require('gulp-concat');
var header = require('gulp-header');
var footer = require('gulp-footer');
var htmlJsStr = require('js-string-escape');

function templateCache(root, base, prefix) {
	if (typeof base !== 'function' && base && base.substr(-1) !== path.sep) {
		base += '/';
	}

	return es.map(function(file, callback) {
		var template = '$templateCache.put("<%= url %>","<%= contents %>");';
		var url;

		file.path = path.normalize(file.path);

		if (typeof base === 'function') {
			url = path.join(root, base(file));
		} else {
			url = path.join(root, file.path.replace(base || file.base, ''));
		}

		if (process.platform === 'win32') {
			url = url.replace(/\\/g, '/');
		}

		if (prefix) {
			url = prefix + url;
		}

		file.contents = new Buffer(gutil.template(template, {
			url: url,
			contents: htmlJsStr(file.contents),
			file: file
		}));

		callback(null, file);
	});
}

module.exports = function(filename, options) {
	if (typeof filename === 'string') {
		options = options || {};
	} else {
		options = filename || {};
		filename = options.filename || 'templates.js';
	}
	if (!options.prefix) {
		options.prefix = 'template:';
	}
	var templateModule;
	if (options.var) {
		templateModule = options.var;
	}
	else {
		templateModule = 'angular.module("<%= module %>"<%= standalone %>)';
	}
	var templateSce = '';
	if (options.prefix) {
		var templateSceFn = function($sceDelegateProvider) {
			function isRegExp(value) {
				try {
					return value instanceof RegExp && value.toString() === '[object RegExp]';
				}
				catch(e) {
					return false;
				}
			}
			function filter(value) {
				return isRegExp(value) ? new RegExp(/^\^*(.*?)\$*$/.exec(value.source)[1]) : value;
			}
			var fn = $sceDelegateProvider.resourceUrlWhitelist;
			fn(fn().map(filter).concat(['#MATCHER#']));
			var map = {};
			fn().forEach(function(v) {
				map[v] = v;
			});
			fn(Object.keys(map).map(function(v) {
				return filter(map[v]);
			}));
		};
		templateSce = templateModule +
			'.config(["$sceDelegateProvider", ' +
			templateSceFn
				.toString()
				.replace(
					'#MATCHER#',
					options.prefix + '**'
				) +
			']);';
	}

	var templateHeader = templateSce + "\n" + templateModule + '.run(["$templateCache", function($templateCache) {';
	var templateFooter = '}]);';

	return es.pipeline(
		templateCache(options.root || '', options.base, options.prefix),
		concat(filename),
		header(templateHeader, {
			module: options.module || 'templates',
			standalone: options.standalone ? ', []' : ''
		}),
		footer(templateFooter)
	);
};
