var gulp = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var ejs = require('gulp-ejs');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var globMulti = require('multi-glob').glob;
var minifycss = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var gulpif = require('gulp-if');
var addsrc = require('gulp-add-src');
var merge = require('merge-stream');
var streamqueue = require('streamqueue');
var ngTemplates = require('./gulp/plugins/ng-templates');
var fs = require('fs');
var path = require('path');

var argv = require('yargs')
	.usage('Usage: $0 [--min]')
	.argv;

var isCompress = function() {
	return argv.min;
};

gulp.task('styles', function() {
	return gulp.src(['web/site/styles/**/*.scss', '!web/site/styles/**/_*.scss'])
		.pipe(sass({errLogToConsole: true}).on('error', gutil.log))
		.pipe(autoprefixer('> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1', 'Explorer 9', 'iOs 6').on('error', gutil.log))
		.pipe(gulpif(isCompress(), minifycss().on('error', gutil.log)))
		.pipe(gulp.dest('web/site/styles'))
		;
});

gulp.task('scripts.ng', function() {
	var tasks = [];
	globMulti(['web/site/scripts/ng/apps/*', 'web/site/scripts/ng/modules/*'], {sync: true}, function(err, dirs) {
		dirs.forEach(function(dir) {
			tasks.push(
				streamqueue({ objectMode: true },
					gulp.src(['module.prefix', 'module.js', '**/*.js'], { cwd: dir }),
					gulp.src(['templates/**/*.ejs'], { cwd: dir })
						.pipe(ejs().on('error', gutil.log))
						.pipe(addsrc(['templates/**/*.html', 'templates/**/*.htm'], { cwd: dir }))
						.pipe(ngTemplates({
							var: 'module'
						}).on('error', gutil.log))
					,
					gulp.src(['module.suffix'], { cwd: dir })
				)
					//.pipe(gulpif(isCompress(), uglify().on('error', gutil.log)))
					.pipe(concat(path.basename(dir) + '.compiled.js'))
					.pipe(gulp.dest(path.join(dir, '..')))
			);
		});

	});
	return merge(tasks);
});

gulp.task('scripts', ['scripts.ng'], function() {
	var tasks = [];
	globMulti(['web/site/scripts/*.json'], {sync: true}, function(err, files) {
		files.forEach(function(file) {
			tasks.push(
				gulp.src(JSON.parse(fs.readFileSync(file)), { cwd: 'web/site/scripts' })
					.pipe(gulpif(isCompress(), uglify().on('error', gutil.log)))
					.pipe(concat(path.basename(file, path.extname(file)) + '.js'))
					.pipe(gulp.dest('web/site/scripts'))
			);
		});

	});
	return merge(tasks);
});


gulp.task('src', ['scripts', 'styles']);

gulp.task('watch', function () {
	gulp.watch([
		'web/site/scripts/**/*.json',
		'web/site/scripts/**/*.html',
		'web/site/scripts/**/*.ejs',
		'web/site/scripts/**/*.js',
		'!web/site/scripts/**/*.compiled.js',
		'!web/site/scripts/*.js'
	], ['scripts']);

	gulp.watch(['web/site/styles/**/*.scss'], ['styles']);
});

gulp.task('build', ['src']);
gulp.task('build-dev', ['src', 'watch']);
